<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Testing\Native;


use Sebwite\Testing\Native\Traits\PHPUnitTrait;

abstract class AbstractTestCase extends \PHPUnit_Framework_TestCase
{
    use PHPUnitTrait;

    public function testTest()
    {
        static::assertTrue(true);
    }
}